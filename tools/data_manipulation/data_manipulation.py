import pandas as pd
import numpy as np


def categorical_feature_validation(df, category_specs):
    """
        Validate categorical values of each feature.

        Returns a data frame that invalid values are replaced with a specific value.

        Parameters
        ----------
        df : data frame, mandatory
            Input is a pandas data frame
        category_specs : dictionary, mandatory
            this parameter is a dictionary th

        Returns
        -------
        result : data frame
            A new data frame with replaced category values replaced with category_specs values in the features columns

        Examples
        --------
        >>> a = pd.DataFrame(
        ...:     [["a", "c", "d"], ["b", "c", "e"], ["b", "c", "f"]], columns=["A", "B", "C"]

    """
    pass

def get_feature_categories(df, columns):
    """
        Returns the categories of each feature in data frame

        Parameters
        ----------
        df : data frame, mandatory
            A data frame that we want to find its feature categories
        columns : array, mandatory
            The name of data frame's columns

        Returns
        -------
        result : dictionary
            Returns the categories of each feature in data frame

        Examples
        --------
        In [1]: from tools.data_manipulation import data_manipulation
        In [2]: import pandas as pd
        In [3]: a = pd.DataFrame(
        ...:     [["a", "c", "d"], ["b", "c", "e"], ["b", "c", "f"]], columns=["A", "B", "C"]
        ...: )

        In [4]: a
        Out[4]: 
        A  B  C
        0  a  c  d
        1  b  c  e
        2  b  c  f

        In [5]: data_manipulation.get_feature_categories(a, ["A", "B", "C"])
        Out[5]: {'A': ['a', 'b'], 'B': ['c'], 'C': ['d', 'e', 'f']}

    """
    result = {}
    for c in list(columns):
        feature = np.array(df[c])
        feature = [str(f) for f in feature]
        result[c] = list(np.unique(feature))
    return result

# from tools.data_manipulation import data_manipulation
# import pandas as pd
# a = pd.DataFrame([['a', 'c', 'd'], ['b', 'c', 'e'], ['b', 'c', 'f']], columns=['A', 'B', 'C'])
# data_manipulation.get_feature_categories(a, ['A', 'B', 'C'])