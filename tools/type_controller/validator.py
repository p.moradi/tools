from tools.type_controller import type_handler as th

import re
class Validator:
    def __init__(self, value) -> None:
        self.value = value
        self.stage = 1
        self.error_message = None
    
    def forward_error(func):
        def wrap(*args, **kwargs):
            self = args[0]
            if self.error_message is not None:
                return self
            result = func(*args, **kwargs)
            return result
        return wrap
    
    def next_stage(self, value):
        valid = Validator(value)
        valid.stage = self.stage + 1
        return valid

    def set_error(self, err_message):
        self.error_message = err_message
        return self

    def get_error(self):
        return self.error_message
    
    @forward_error
    def satisfy_regex(self, regex_str):
        regex = re.compile(regex_str)
        match_res = re.match(regex, self.value)
        if match_res:
            return self.next_stage(self.value)
        # broadcast error
        error_message = 'Failed to match regex expression'
        return self.set_error(error_message)
         
    """
    this function gets a string and converts it to numeric if it is applicable
    options: A list of values as options
    """
    @forward_error
    def is_numeric(self, numeric_type=['float', 'int']):
        if str(self.value.__class__.__name__) in numeric_type:
            return self.next_stage(self.value)
        str_type = th.get_string_type(self.value)
        if str_type in numeric_type:
            value = th.string_to_number(self.value)
        else:
            error_message = f'Type of {self.value} is not in {numeric_type} list'
            return self.set_error(error_message)

        return self.next_stage(value)
    
    @forward_error
    def in_range(self, range_tpl):
        response = th.check_in_range(self.value, range_tpl)
        if not response:
            # broadcast error
            error_message = f'{self.value} is not in range {str(range_tpl)}'
            return self.set_error(error_message)

        return self.next_stage(self.value)
    
    @forward_error
    def generic_function(self, func):
        return self.next_stage(func(self.value))
    
    """
    this function checks if value is in option list
    options: A list of values as options
    """
    @forward_error
    def check_option_list(self, options):
        if self.value in options:
            return self.next_stage(self.value)
        error_message = f'{self.value} is not in {options}'
        return self.set_error(error_message)
    
    def get(self):
        result = {
            'status': 'SUCCESS',
            'value': self.value,
            'stage': self.stage,
            'error_message': self.get_error()
        }
        if self.get_error() is not None:
            result['status'] = 'ERROR'
        else:
            result['status'] ='SUCCESS'
        return result

# a = '111'
# res = Validator(a).is_numeric().in_range((1,100)).get()
# print(res)
        