import re

"""
Gets a string and check it is a float, int or nonnumerical string
"""
def get_string_type(txt):
    # Check if the string is float
    num_format = re.compile("[\-]?[1-9][0-9]*\.[0-9]*$")
    is_float = re.match(num_format, txt)
    if is_float:
        return 'float'
    
    # Check if the string is int
    num_format = re.compile("[\-]?[1-9][0-9]*$")
    is_float = re.match(num_format, txt)
    if is_float:
        return 'int'
    
    return 'string'

"""
Gets a string and convert it to numeric if applicable
"""
def string_to_number(text):
    str_type = get_string_type(text)
    if str_type == 'float':
        return float(text)
    elif str_type == 'int':
        return int(text)
    elif str_type == 'string':
        raise Exception("Cannot convert string to number")

